/******************************************************************************/
/********************************* DATA ***************************************/
/******************************************************************************/

	var square = ["Commence par A","Commence par C","Commence par T","Commence par M","Commence par P","Finit par É","Commence par F","Commence par E","Commence par I/J/K/H","Commence par G","Finit par ON","Commence par B","Ne contient pas O","Commence par D","Finit par A","Fint par QUE","Commence par R","Commence par S","Ne contient pas A"];

	var circle = ["Fruit","Santé","Qui se boit","Invention","Qui se boit","Marque","Électrique","Santé","Légumes","Dessert","Qui se boit","Invention","Plantes / Végétaux","Véhicule","À table","Véhicule","Fleuves / Montagnes / deserts","Animaux","Qui tourne","Horreur","Sexuel","Outil"];

	var star = ["Chanteur / Groupe de musique","Titre de film","Personnage","Walt Disney","Insectes et assimilés","Pays","Métiers","Loisirs / activités","Villes","Sports","Héros","Télévision","Vilain","Jeux de société","Oeuvres d'Art","Prénom"];

  var hexagon = ["Bois","Gros","Marin / Aquatique","Livre","Vert","Grand","Tissu","Niais","Volant","Minéral / Pierre","Coloré","Dangereux","Qui pue","Froid","Série TV / BD / Dessin animé","Métal","Original","Triste"];

  var triangle = ["Temps","Dans une maison","Recyclable","Nuit","Coupant","Dans le jardin","Imaginaire","Printemps / Été","Pour les enfants","En forêt","À la ferme","Comestible","Musique","Romantique","Historique / religieux","Matin","Automne / Hiver","Qui flotte","Dans une maison","Parfumé"];


/******************************************************************************/
/********************************* FUNCTIONS **********************************/
/******************************************************************************/

	function getRandomInt(min, max) {
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function alea(liste) {
		var numMax = liste.length - 1;
		var aleaNbr = getRandomInt(0, numMax);
		return liste[aleaNbr];
	}

  function drawcard(type) {
    let temp = "";
    switch(type) {
      case 'square':
        var card = document.querySelectorAll('.square h2');
        var count = document.querySelector('.square .articles__footer p');
        var countvalue = square.length;
        if (square.length > 0)
          temp = alea(square);
        square = square.filter(function(value, index, arr){ return value != temp;});
        break;
      case 'circle':
        var card = document.querySelectorAll('.circle h2');
        var count = document.querySelector('.circle .articles__footer p');
        var countvalue = circle.length;
        if (circle.length > 0)
          temp = alea(circle);
        circle = circle.filter(function(value, index, arr){ return value != temp;});
        break;
      case 'star':
        var card = document.querySelectorAll('.star h2');
        var count = document.querySelector('.star .articles__footer p');
        var countvalue = star.length;
        if (star.length > 0)
          temp = alea(star);
        star = star.filter(function(value, index, arr){ return value != temp;});
        break;
      case 'hexagon':
        var card = document.querySelectorAll('.hexagon h2');
        var count = document.querySelector('.hexagon .articles__footer p');
        var countvalue = hexagon.length;
        if (hexagon.length > 0)
          temp = alea(hexagon);
        hexagon = hexagon.filter(function(value, index, arr){ return value != temp;});
        break;
      case 'triangle':
        var card = document.querySelectorAll('.triangle h2');
        var count = document.querySelector('.triangle .articles__footer p');
        var countvalue = triangle.length;
        if (triangle.length > 0)
          temp = alea(triangle);
        triangle = triangle.filter(function(value, index, arr){ return value != temp;});
        break;
    }
    card[0].innerHTML = temp;
    card[1].innerHTML = temp;
    count.innerText = countvalue;
  }


	function drawallcards()
		{
      drawcard('square');
      drawcard('circle');
      drawcard('star');
      drawcard('hexagon');
      drawcard('triangle');
		}

  function refreshcardcounts() {
    var squarecount = document.querySelector('.square .articles__footer p');
    var circlecount = document.querySelector('.circle .articles__footer p');
    var starcount = document.querySelector('.star .articles__footer p');
    var hexagoncount = document.querySelector('.hexagon .articles__footer p');
    var trianglecount = document.querySelector('.triangle .articles__footer p');
    squarecount.innerText = square.length;
    circlecount.innerText = circle.length;
    starcount.innerText = star.length;
    hexagoncount.innerText = hexagon.length;
    trianglecount.innerText = triangle.length;
  }

  var currenttimer;
  function starttimer() {
    var display = document.querySelector('#timer');
    if (display.classList.contains('started')) {
      clearInterval(currenttimer);
      display.classList.remove('started');
      display.innerText = "05:00";
      document.querySelector('#launchtimer').innerText = "Lancer le chrono";
    }
    else {
      var fiveMinutes = 60 * 5 - 1;
      var duration = fiveMinutes;
      var timer = duration, minutes, seconds;
      currenttimer = setInterval(countdown, 1000);
      function countdown(){
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.textContent = minutes + ":" + seconds;
        if (--timer < 0) {
            timer = duration;
        }
      }
      display.classList.add('started');
      document.querySelector('#launchtimer').innerText = "Stopper le chrono";
  }
  }

    /******************************************************************************/
    /********************************* EVENTS *************************************/
    /******************************************************************************/

	document.addEventListener('DOMContentLoaded', function()
	{
		// récupération des éléments HTML
		var drawcards = document.querySelector('#drawcards');
    var drawsquare = document.querySelector('.square');
    var drawcircle = document.querySelector('.circle');
    var drawstar = document.querySelector('.star');
    var drawhexagon = document.querySelector('.hexagon');
    var drawtriangle = document.querySelector('.triangle');
    var launchtimer = document.querySelector('#launchtimer');

    refreshcardcounts();

		drawcards.addEventListener('click', function(event){ drawallcards(); });
    drawsquare.addEventListener('click', function(event){ drawcard('square');});
    drawcircle.addEventListener('click', function(event){ drawcard('circle');});
    drawstar.addEventListener('click', function(event){ drawcard('star');});
    drawhexagon.addEventListener('click', function(event){ drawcard('hexagon');});
    drawtriangle.addEventListener('click', function(event){ drawcard('triangle');});

    launchtimer.addEventListener('click',function(event){ starttimer();});

	});
